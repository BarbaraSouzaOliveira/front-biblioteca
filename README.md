## Configuração da Máquina para o Front-end

Nota: Caso queira ler o readme pelo docs acesse o link https://docs.google.com/document/d/1iSSS8oisozdLIpK5YnKQlYwFL_x_XZBV3QRTnOIOI4s/edit?usp=sharing 

## Especificações do projeto

Neste projeto foi usado o ReactJS para o front-end.
Biblioteca de desing: ANTDesing https://ant.design/docs/react/introduce

## Chocolatey

Chocolatey é um gerenciador de pacotes para o windows. Sua instalação é bem simples, pelo powershell (em modo administrador) rode o comando Get-ExecutionPolicy  se retornar Restricted rode Set-ExecutionPolicy Bypass -Scope Process. 
Em seguida rode o comando 
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))  
 Após isso, o chocolatey estará instalado na sua máquina, finalize o powershell.

## Yarn 

No  PowerShell em modo administrador rode o comando choco install yarn

## Repositório

Após clonar o repositório  em sua máquina na pasta do repositório rode o comando através do terminal yarn start.

## Comando para rodar o projeto

Para rodar o projeto, faça todo o passo a passo do backend, após o back-end estar rodando o server, rode o comando  yarn start.

## Login

Caso tenha feito a restauração do banco de dados (como ensinado no README do api-biblioteca) o Login se dá com email : teste@email.com
senha: 12345678. Caso contrário se faz necessário a inserção manual de um usuário na tabela “usuarios” do banco de dados, visto que o projeto não contempla cadastro de usuários.


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
