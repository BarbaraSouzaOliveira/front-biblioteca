import React from 'react'
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom'
import Home from '../pages/Home'
import Form from '../pages/Form'
import GenreForm from '../pages/Form/genre'
import PublisherForm from '../pages/Form/publisher'
import AuthorForm from '../pages/Form/author'
import BookForm from '../pages/Form/book'
import Login from '../pages/Login'

const token = localStorage.getItem('token')
const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            (token) ? (<Component {...props} />) : (
                <Redirect to={{pathname: "/", state: {from: props.location}}}/>
            )
        }
    />
);
const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path={'/'} component={Login} />        
            <PrivateRoute  path={'/Home'} component={Home}/>
            <PrivateRoute path={'/Formulario'} component={Form}/>
            <PrivateRoute path={'/formularioGenero/:id'} component={GenreForm}/>
            <PrivateRoute path={'/formularioEditora/:id'} component={PublisherForm}/>
            <PrivateRoute path={'/formularioAutor/:id'} component={AuthorForm}/>
            <PrivateRoute path={'/formularioLivro/:id'} component={BookForm}/>
        </Switch>
        
        
      
    </BrowserRouter>
)
export default Routes