import API, {toFormData} from "./Api"

export default class BookService {
  static async getAll(params) {
    return await API.get(`/book`, { params })
  }

  static async get(id) {
    return await API.get(`/book/${id}`)
  }

  static async create(data) {
    return await API.post(`/book`, data)
  }

  static async update(id, data) {
    return await API.post(`/book/${id}`, data)
  }

  static async delete(id) {
    return await API.delete(`/book/${id}`)
  }

  static async save(data, idAttribute = "id") {
    const id = data[idAttribute];

    let url = "/book"
    let method = "post"

    if (id) {
      url += `/${id}`
      method = "post"
    }

    return await API[method](url, toFormData(data))
  }
}
