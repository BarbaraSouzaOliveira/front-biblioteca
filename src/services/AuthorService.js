import API from "./Api"

export default class AuthorService {
  static async getAll(params) {
    return await API.get(`/author`, { params })
  }

  static async get(id) {
    return await API.get(`/author/${id}`)
  }

  static async create(data) {
    return await API.post(`/author`, data)
  }

  static async update(id, data) {
    return await API.put(`/author/${id}`, data)
  }

  static async delete(id) {
    return await API.delete(`/author/${id}`)
  }

  static async save(data, idAttribute = "id") {
    const id = data[idAttribute]

    let url = "/author"
    let method = "post"

    if (id) {
      url += `/${id}`
      method = "put"
    }

    return await API[method](url, data)
  }
}
