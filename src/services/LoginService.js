import API from "./Api"

export default class Login{
  static async login(params) {
    return await API.post(`/login`, { params })
  }
  
}
