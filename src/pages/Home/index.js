import React from 'react'
import Page from '../Page'
import {Tabs, Icon, Tooltip} from 'antd'
import {Link} from 'react-router-dom'
import Header from '../../components/Header'
import Book from '../../components/Book'
import ListGenre from '../../components/Genre'
import ListAuthor from '../../components/Author'
import ListPublisher from '../../components/Publisher'
import bookService from '../../services/BookService'
import authorService from '../../services/AuthorService'
import publisherService from '../../services/PublisherService'
import genreService from '../../services/GenreService'
import './style.css'

class Home extends Page{
    state={
        books:[],
        authors:[],
        genres:[],
        publishers:[]
    }
    componentDidMount() {       
        super.componentDidMount();
        bookService.getAll().then(response => {
          this.update({ books: response.data });
        })
        authorService.getAll().then(response => {
            this.update({ authors: response.data });
        })
        genreService.getAll().then(response => {
            this.update({ genres: response.data });
        })
        publisherService.getAll().then(response => {
            this.update({ publishers: response.data });
        })
      }
    AuthorName (id) {
        const {authors} = this.state
        var author = authors.filter(e => +e.id === +id )
        return(author[0])    
    }
    GenreType (id) {
        const {genres} = this.state
        var genre = genres.filter(e => +e.id === +id )
        return(genre[0])    
    }
    PublisherName (id) {
        const {publishers} = this.state
        var publisher = publishers.filter(e => +e.id === +id )
        return(publisher[0])    
    }
    render(){
        const {TabPane} = Tabs
        const {books} = this.state

        return(
            <>
                <Header/>
                <div className="homeBody">
                    <Tabs size="large" defaultActiveKey="1">
                        <TabPane 
                            tab={<span><Icon type="read" />Livros</span>}
                            key="1"
                        >
                        {(books.length===0)?
                        
                        (
                            <>                    
                                <h4 style={{color:"grey"}}> Não há livros, Insira um Novo:</h4>
                                <Tooltip placement="bottomLeft" title="Adicionar Editora"> 
                                    <Link to="/formulariolivro/0">
                                        <Icon type="plus"/>
                                    </Link>
                                </Tooltip>
                            </>
                        )
                        : null}
                        <div className="List">                        
                        {books.map(book=>{
                            var url = book.book_image    
                            if(url != null){
                            url= url.replace('public', 'http://127.0.0.1:8000/storage')
                            }
                           if(this.AuthorName(book.id_author)){var authorName = this.AuthorName(book.id_author).author_name}
                           if(this.PublisherName(book.id_publisher)){var publisherName = this.PublisherName(book.id_publisher).publisher_name}
                           if(this.GenreType(book.id_genre)){var genreType = this.GenreType(book.id_genre).genre_type}
                           
                            
                            return(
                                <Book 
                                title={book.book_title} 
                                image={url} 
                                id={book.id} 
                                year={book.book_published}
                                publisher={publisherName}
                                genre={genreType}
                                author={authorName}
                                />
                            )
                        })}
                        
                        </div>                        
                        </TabPane>
                        <TabPane 
                            tab={<span><Icon type="user" />Autores</span>}
                            key="2"
                        >
                        <ListAuthor />
                        </TabPane>
                        <TabPane 
                            tab={<span><Icon type="edit" />Editoras</span>}
                            key="3"
                        >
                        <ListPublisher />
                        </TabPane>
                        <TabPane 
                            tab={<span><Icon type="robot" />Genero</span>}
                            key="4"
                        >
                        <ListGenre />
                        </TabPane>
                    </Tabs>
                </div>
            </>
        )
    }
}export default Home 