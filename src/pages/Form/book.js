import React from 'react'
import {Input, Button, notification, Select } from 'antd'
import Page from '../Page'
import bookService from '../../services/BookService'
import authorService from '../../services/AuthorService'
import genreService from '../../services/GenreService'
import publisherService from '../../services/PublisherService'
import Header from '../../components/Header'
class Book extends Page{
    service = bookService
    state= {
        model: {},
        authors:[],
        genres:[],
        publishers:[],
        idBook: 0

    }
    componentWillMount() {
      this.getModel()
    }
  
    componentDidMount() {       
        super.componentDidMount();
        this.setState({idBook: this.props.match.params.id})
        authorService.getAll().then(response => {
          this.update({ authors: response.data });
        })
        publisherService.getAll().then(response => {
          this.update({ publishers: response.data });
        })        
        genreService.getAll().then(response => {
            this.update({ genres: response.data });
          })
      }
    

    
    Submit = e => {
        e.preventDefault()
             
        bookService.save( this.state.model,"id").then(response => {
          this.update({ model: response.data })
          notification.open({
            message: "Sucesso!",
            description: "Seus Dados foram salvos com sucesso."
          })
          this.props.history.push("/Formulario")
        }).catch(e =>{
          notification.open({
            message:"Erro",
            description: "Algo deu errado tente novamente"
          })
          
      })
      
      }
      
    render(){
        const {model,idBook} = this.state
        const {Option} = Select
        const {authors, genres, publishers}= this.state        
        console.log(idBook)
        return(
          <>
            <Header />
            <div className="FormBox">
              <h1 style={{textAlign:"center"}}>Adicionar novo Livro</h1>
                <h3>*Titulo do Livro</h3>            
                <Input
                    style={{width:"45%", marginRight:10}}
                    name='book_title'
                    placeholder={model.book_title || 'Titulo do livro'}
                    onChange={e => this.InputChange(e, model)}
                    max={50}
                    required
                />
                <h3>*Ano de Publicação</h3>            
                <Input
                    style={{width:"45%", marginRight:10}}
                    name='book_published'
                    placeholder={model.book_published || 'DD/MM/AAAA'}
                    onChange={e => this.InputChange(e, model)}
                    required
                />
                <h3>Capa do livro</h3>                
                <h6 style={{color:"grey"}}>Tamanho recomendado (310x446)</h6> 
                {(+idBook!==0)? <h5 style={{color:"grey"}}>É necessario reinserir a imagem</h5>: null}           
                <Input
                    type="file"
                    style={{width:"45%", marginRight:10}}
                    name='book_image'
                    placeholder={model.book_image || 'imagem da capa do livro'}
                    onChange={e => this.InputChange(e, model)}
                />
                <h3>*Autor</h3>
                <Select
                    placeholder={model.id_author || "Autor"}
                    style={{width:200,  marginRight:10}}
                    value={model.id_author}
                    onChange={value =>
                      this.InputChange(
                        { target: { name: "id_author", value } },
                        this.state.model
                      )
                    }
                    
                  >
                {authors.map(atr=>{
                    return(
                        <Option value={atr.id}>{atr.author_name}</Option>
                    )
                })} 
                </Select>
                <h3>*Genero Literario</h3>
                <Select
                    placeholder={model.id_genre || "Genero Literario"}
                    style={{width:200,  marginRight:10}}
                    value={model.id_genre}
                    onChange={value =>
                      this.InputChange(
                        { target: { name: "id_genre", value } },
                        this.state.model
                      )
                    }
                  >
                {genres.map(gne=>{
                    return(
                        <Option value={gne.id}>{gne.genre_type}</Option>
                    )
                })} 
                </Select>
                <h3>*Editora</h3>
                <Select
                    placeholder={model.id_publisher || "Editora"}
                    style={{width:200,  marginRight:10}}
                    value={model.id_publisher}
                    onChange={value =>
                      this.InputChange(
                        { target: { name: "id_publisher", value } },
                        this.state.model
                      )
                    }
                  >
                {publishers.map(pbs=>{
                    return(
                        <Option value={pbs.id}>{pbs.publisher_name}</Option>
                    )
                })} 
                </Select>
                <Button 
                    onClick={this.Submit}
                >
                 Enviar 
                </Button>
                <h4 style={{color:"grey", textAlign:"center"}}>*Campo Obrigatorio</h4>                      
            </div>
          </>  
        )
    }
}export default Book