import React from 'react'
import Page from '../Page'
import{Link} from 'react-router-dom'
import {Tooltip,Avatar} from 'antd'
import Header from '../../components/Header'
import bookAdd from '../../assets/book-add.png'
import authorAdd from '../../assets/author-add.jpg'
import genreAdd from '../../assets/genre-add.jpg'
import publisherAdd from '../../assets/publisher-add.jpg'
import './style.css'
class Form extends Page{
    render(){
        return(
            <>
            <Header />
            <div className="formBody">
            <Tooltip placement="bottomLeft" title="Adicionar Livro"> 
                <Link to="/formularioLivro/0">
                    <Avatar style={{width:150, height:150}} shape="square" src={bookAdd}/>
                </Link>
            </Tooltip>
            <Tooltip placement="bottomLeft" title="Adicionar Editora">     
                <Link to="/formularioEditora/0">
                    <Avatar style={{width:150, height:150}} shape="square" src={publisherAdd}/>
                </Link>
            </Tooltip>
            <Tooltip placement="bottomLeft" title="Adicionar Autor">     
                <Link to="/formularioAutor/0">
                    <Avatar style={{width:150, height:150}} shape="square" src={authorAdd}/>
                </Link>
            </Tooltip>
            <Tooltip placement="bottomLeft" title="Adicionar Genero">     
                <Link to="/formularioGenero/0">
                    <Avatar style={{width:150, height:150}} shape="square" src={genreAdd}/>
                </Link>
            </Tooltip>
            </div>    
            </>
        )
    }
}export default Form