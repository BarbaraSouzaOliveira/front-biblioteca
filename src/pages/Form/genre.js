import React from 'react'
import {Input, Button, notification} from 'antd'
import Page from '../Page'
import genreService from '../../services/GenreService'
import Header from '../../components/Header'
class Genre extends Page{
    service = genreService 
    state= {
        model: {}
    }
    componentWillMount() {
      this.getModel()
    }
  
    Submit = e => {
        e.preventDefault()
        console.log(this.state.model)
        genreService.save( this.state.model,"id").then(response => {
          this.update({ model: response.data })
          notification.open({
            message: "Sucesso!",
            description: "Seus Dados foram salvos com sucesso."
          })
          this.props.history.push("/Formulario")
        }).catch(e =>{
          notification.open({
            message:"Erro",
            description: "Algo deu errado tente novamente"
          })
          
      })
      
      }
      
    render(){
        const {model} = this.state
        return(
          <>
            <Header />
            <div className="FormBox">
              <h1 style={{textAlign:"center"}}>Adicionar novo Genero Literario</h1>
                <h3>*Nome do Genero  Literario</h3>            
                <Input
                    style={{width:"45%", marginRight:10}}
                    name='genre_type'
                    model={model.genre_type} 
                    placeholder={model.genre_type || 'Genero Literario'}
                    onChange={e => this.InputChange(e, model)}
                    max={50}
                    required
                />
                <Button 
                    onClick={this.Submit}
                >
                 Enviar 
                </Button>   
                <h4 style={{color:"grey", textAlign:"center"}}>*Campo Obrigatorio</h4>                     
            </div>
          </>  
        )
    }
}export default Genre