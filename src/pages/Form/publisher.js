import React from 'react'
import {Input, Button, notification} from 'antd'
import Page from '../Page'
import publisherService from '../../services/PublisherService'
import Header from '../../components/Header'
class Publisher extends Page{
    service = publisherService
    state= {
        model: {}
    }
    componentWillMount() {
      this.getModel()
    }
  
    Submit = e => {
        e.preventDefault()
        console.log(this.state.model)
        publisherService.save( this.state.model,"id").then(response => {
          this.update({ model: response.data })
          notification.open({
            message: "Sucesso!",
            description: "Seus Dados foram salvos com sucesso."
          })
          this.props.history.push("/Formulario")
        }).catch(e =>{
          notification.open({
            message:"Erro",
            description: "Algo deu errado tente novamente"
          })
          
      })
      
      }
      
    render(){
        const {model} = this.state
        console.log(model)
        return(
          <>
            <Header />
            <div className="FormBox">
              <h1 style={{textAlign:"center"}}>Nova Editora</h1>
                <h3>*Nome da Editora</h3>            
                <Input
                    style={{width:"45%", marginRight:10}}
                    name='publisher_name'
                    model={model.publisher_name} 
                    placeholder={model.publisher_name || 'nome  editora'}
                    onChange={e => this.InputChange(e, model)}
                    max={50}
                    required
                />
                <Button 
                    onClick={this.Submit}
                >
                 Enviar 
                </Button>
                <h4 style={{color:"grey", textAlign:"center"}}>*Campo Obrigatorio</h4>                        
            </div>
          </>  
        )
    }
}export default Publisher