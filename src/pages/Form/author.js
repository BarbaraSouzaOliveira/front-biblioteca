import React from 'react'
import {Input, Button, notification, Select} from 'antd'
import Page from '../Page'
import authorService from '../../services/AuthorService'
import Header from '../../components/Header'
class Author extends Page{
    service =authorService
    state= {
        model: {}
    }
    componentWillMount() {
      this.getModel()
    }  
    Submit = e => {
        e.preventDefault()
        console.log(this.state.model)
        authorService.save( this.state.model,"id").then(response => {
          this.update({ model: response.data })
          notification.open({
            message: "Sucesso!",
            description: "Seus Dados foram salvos com sucesso."
          })
          this.props.history.push("/Formulario")
        }).catch(e =>{
          notification.open({
            message:"Erro",
            description: "Algo deu errado tente novamente"
          })
          
      })
      
      }
      
    render(){
        const {model} = this.state
        const {Option} = Select
        return(
          <>
            <Header />
            <div className="FormBox">
              <h1 style={{textAlign:"center"}}>Adicionar novo(a) Autor(a)</h1>
                <h3>*Nome do(a) Autor(a)</h3>            
                <Input
                    style={{width:"45%", marginRight:10}}
                    name='author_name'
                    model={model.author_name} 
                    placeholder={model.author_name || 'Nome do Autor'}
                    onChange={e => this.InputChange(e, model)}
                    max={50}
                    required
                />
                <h3>*Nacionalidade do(a) Autor(a)</h3>            
                <Input
                    style={{width:"45%", marginRight:10}}
                    name='author_nationality'
                    model={model.author_nationality} 
                    placeholder={model.author_nationality || 'Nacionalidade do Autor'}
                    onChange={e => this.InputChange(e, model)}
                    max={50}
                    required
                />
                <h3>*Ano de nascimento do(a) Autor(a)</h3>            
                <Input
                    style={{width:"45%", marginRight:10}}
                    name='author_born'
                    model={model.author_born} 
                    placeholder={model.author_born || 'Ano de nacimento do Autor'}
                    onChange={e => this.InputChange(e, model)}
                    max={50}
                    required
                />
                <h3>*Genero Autor(a)</h3> 
                <Select
                    placeholder={model.author_genre || "genero Autor"}
                    style={{width:200,  marginRight:10}}
                    value={model.author_genre}
                    onChange={value =>
                      this.InputChange(
                        { target: { name: "author_genre", value } },
                        this.state.model
                      )
                    }
                  >
                    <Option value="Feminino">Feminino</Option>
                    <Option value="Masculino">Masculino</Option>
                    <Option value="outro">Outro</Option>
                  </Select>

                <Button 
                    onClick={this.Submit}
                >
                 Enviar 
                </Button> 
                <h4 style={{color:"grey", textAlign:"center"}}>*Campo Obrigatorio</h4>                     
            </div>
          </>  
        )
    }
}export default Author