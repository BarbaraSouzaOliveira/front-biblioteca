import React from 'react'
import {Input, Button, Form, notification} from 'antd'
import Page from '../Page'
import LoginService from '../../services/LoginService'
import './style.css'

class Login extends Page{   
    state ={
        model:{}
    } 

    submit = e =>{
        const {model} = this.state
        e.preventDefault()
        LoginService.login(model).then(response => {
            localStorage.setItem('token', response.data)
            notification.open({
                message: "Login efetuado com sucesso"
            })
            this.props.history.push("/Home")
        }).catch(e =>{
            notification.open({message: "Erro, algo deu errado!"})
        })
        
        

    }
    render(){
        const {form} = this.props
        const {model} = this.state
        const {getFieldDecorator} = form
        return(
            <div className="LoginBox">
                <h2 style={{textAlign:"center"}}>Login</h2>
                <Form onSubmit={this.submit}>
                    <Form.Item>
                        {getFieldDecorator('Email',{
                            rules:[
                                {required: true , message: "Campo obrigatorio"}
                            ]
                        })}
                        <Input 
                            placeholder="E-mail"
                            name="email"
                            onChange={e => this.InputChange(e, model )}
                        />
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('senha',{
                            rules:[
                                {required: true , message: "Campo obrigatorio"}
                            ]
                        })}
                        <Input.Password
                            placeholder="Senha"
                            name="senha"
                            onChange={e => this.InputChange(e, model )}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button className="LoginButton" htmlType="submit">Logar</Button>
                    </Form.Item>
                </Form>
            </div>
        )
    }
}export default Form.create({name: "LoginForm"}) (Login)