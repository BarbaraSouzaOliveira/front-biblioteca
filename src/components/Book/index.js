import React from 'react'
import noPhoto from '../../assets/no-photo.jpg'
import './style.css'
import { Button, Tooltip, Modal,} from 'antd'
import {Link} from 'react-router-dom'
import bookService from '../../services/BookService'
class Book extends React.Component{
    state = { 
        visible: false 
    }
    update = (state = this.state) => {
        return this._canUpdate && this.setState(state)
    }
    

    showModal = () => {
      this.setState({
        visible: true,
      })
    }
  
    cancel = e => {
      console.log(e)
      this.setState({
        visible: false,
      })
    }
  
    render(){
        const{image, title, id, year, genre, author, publisher} = this.props
        return(
            <div className="bookCard">
                <img 
                    alt="capa" 
                    src={image || noPhoto} 
                    width="200" 
                    height="250"    
                />
                <div className="bookInfo">
                    <h3 style={{textAlign: "center"}}>{title}</h3>
                    <div className="buttonGroup">
                    <Tooltip placement="bottomLeft" title="editar">                        
                        <Link                                
                            to={"FormularioLivro/" + id}
                        >
                            <Button icon="edit"/>
                        </Link>
                    </Tooltip>
                    <Tooltip placement="bottomLeft" title="excluir">  
                    <Button
                     icon="delete"
                    onClick={e => {
                        e.preventDefault()
                        bookService.delete(id).then(response => {
                        bookService.getAll().then(response => {
                            this.update({ books: response.data })
                            
                        })
                        })
                        window.location.reload()
                    
                    }}
          />
           
          

                    </Tooltip>
                    <Tooltip placement="bottomLeft" title="mais Informações"> 
                        <Button icon="plus" onClick={this.showModal}/>
                    </Tooltip>
                    </div>

                </div>

                <Modal
          title={title}
          visible={this.state.visible}
          onCancel={this.cancel}
          footer={null}
        >
                <p>Ano de Lançamento: {year}</p>
                <p>Autor(a): {author}</p>
                <p>Editora: {publisher}</p>
                <p>Genero literario: {genre}</p>
        </Modal>
            </div>
        )
    }
}export default Book