import React from   'react'
import {Tooltip, Icon,Button,notification} from 'antd'
import {Link} from 'react-router-dom'
import genreService from'../../services/GenreService'
import './style.css'

class Genre extends React.Component{
    state={
        genre: [],
    }
       componentDidMount() {       
           this._canUpdate = true     
           genreService.getAll().then(response => {
               this.update({ genre: response.data })
           })
         }
    update = (state = this.state) => {
        return this._canUpdate && this.setState(state)
    }
    render(){
        const {genre}= this.state
        if (genre.length === 0){
            return(
                <>                    
                    <h4 style={{color:"grey"}}> Não há generos, Insira um novo:</h4>
                    <Tooltip placement="bottomLeft" title="Adicionar genero"> 
                        <Link to="/formularioGenero/0">
                            <Icon type="plus"/>
                        </Link>
                    </Tooltip>
                </>
            )
        }
        return(<>
             <div >{    
                genre.map(gns =>{
                    return(
                        <div className="GenreCard">
                        <h3 class="GenreText">{gns.genre_type}</h3>
                        <Button
                            icon="delete"
                            onClick={e => {
                            e.preventDefault()
                            genreService.delete(gns.id).then(response => {
                            genreService.getAll().then(response => {
                            this.update({ genre: response.data })
                            notification.open({
                                message: "Sucesso!",
                                description: "Seus Dados foram excluidos com sucesso."
                              })
                            })
                            }).catch(e =>{
                                notification.open({
                                  message:"Erro",
                                  description: "Verifique se os dados estão vinculados a algum livro "
                                })})
                            }}
                        />
                        <Link                                
                            to={"FormularioGenero/" + gns.id}
                        >
                            <Button 
                                class="GenreButton"
                                icon="edit"
                            />
                        </Link>
                        </div>
                        )
                 })}
          </div>
        </>)      
    }
}export default Genre