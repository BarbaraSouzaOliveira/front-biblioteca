import React from 'react'
import {Link} from 'react-router-dom'
import {Button} from 'antd'
import './style.css'

class Header extends React.Component{
    render(){
        return(
            <div class="header">
                <Link to="/Home">
                    <a href="#default" class="logo">Biblioteca</a>
                </Link>
                <div class="header-right">
                <Link to="/formulario">
                    <Button icon="setting" />
                </Link>
                </div>
            </div>
        )
    }
}export default Header