import React from   'react'
import {Tooltip, Icon,Button, Card,notification} from 'antd'
import {Link} from 'react-router-dom'
import authorService from'../../services/AuthorService'

class Author extends React.Component{
    state={
        author: [],
    }
       componentDidMount() {       
           this._canUpdate = true     
           authorService.getAll().then(response => {
               this.update({ author: response.data })
           })
         }
    update = (state = this.state) => {
        return this._canUpdate && this.setState(state)
    }
    render(){
        const {author}= this.state
        if (author.length === 0){
            return(
                <>                    
                    <h4 style={{color:"grey"}}> Não há Autores, Insira um(a) novo(a):</h4>
                    <Tooltip placement="bottomLeft" title="Adicionar Autor"> 
                        <Link to="/formularioAutor/0">
                            <Icon type="plus"/>
                        </Link>
                    </Tooltip>
                </>
            )
        }
        return(        
             <div className="List">             
             {author.map(atr =>{
                 return(
                     <Card
                     style={{width:"33%", marginTop:"10px"}}
                     title={atr.author_name}
                     >
                         <p>Nacionalidade: {atr.author_nationality}</p>
                         <p>Genero: {atr.author_genre}</p>                                    
                         <p>Data Nascimento: {atr.author_born}</p>
                         <Button
                            icon="delete"
                            onClick={e => {
                            e.preventDefault()
                            authorService.delete(atr.id).then(response => {
                            authorService.getAll().then(response => {
                            this.update({ authors: response.data })
                            notification.open({
                                message: "Sucesso!",
                                description: "Seus Dados foram excluidos com sucesso."
                              })
                            })
                            }).catch(e =>{
                                notification.open({
                                  message:"Erro",
                                  description: "Verifique se os dados estão vinculados a algum livro "
                                })})
                            }}
                        />
                        <Link                                
                            to={"FormularioAutor/" + atr.id}
                        >
                            <Button icon="edit"/>
                        </Link>
                     </Card> 
                 )
             })}
             </div>
             
             )
               
    }
}export default Author