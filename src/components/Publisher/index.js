import React from   'react'
import {Tooltip, Icon,Button, notification} from 'antd'
import {Link} from 'react-router-dom'
import publisherService from'../../services/PublisherService'
import './style.css'

class Publisher extends React.Component{
 state={
     publisher: [],
 }
    componentDidMount() {       
        this._canUpdate = true     
        publisherService.getAll().then(response => {
            this.update({ publisher: response.data })
        })
      }
    update = (state = this.state) => {
        return this._canUpdate && this.setState(state)
    }
    render(){
        const {publisher}=  this.state
        if (publisher.length === 0){
            return(
                <>                    
                    <h4 style={{color:"grey"}}> Não há Editoras, Insira uma Nova:</h4>
                    <Tooltip placement="bottomLeft" title="Adicionar Editora"> 
                        <Link to="/formularioEditora/0">
                            <Icon type="plus"/>
                        </Link>
                    </Tooltip>
                </>
            )
        }
        return(<>
             <div>{    
                publisher.map(pbs =>{
                    return(
                        <div className="PublisherCard" >
                        <h3 className="PublisherText" >{pbs.publisher_name}</h3>
                        <Button                           
                            icon="delete"
                            onClick={e => {
                            e.preventDefault()
                            publisherService.delete(pbs.id).then(response => {
                            publisherService.getAll().then(response => {
                            this.update({ publisher: response.data })
                            notification.open({
                                message: "Sucesso!",
                                description: "Seus Dados foram excluidos com sucesso."
                              })
                            })
                            }).catch(e =>{
                                notification.open({
                                  message:"Erro",
                                  description: "Verifique se os dados estão vinculados a algum livro "
                                })})
                            }}
                        />
                        <Link                                
                            to={"FormularioEditora/" + pbs.id}
                        >
                            <Button 
                                className="PublisherButton"
                                icon="edit"
                                />
                        </Link>

                        </div>
                        )
                 })}
          </div>
        </>)      
    }
}export default Publisher